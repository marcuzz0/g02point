<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyDrawingTol="1" symbologyReferenceScale="-1" readOnly="0" version="3.28.1-Firenze" hasScaleBasedVisibilityFlag="0" maxScale="0" styleCategories="AllStyleCategories" simplifyDrawingHints="1" simplifyLocal="1" simplifyMaxScale="1" simplifyAlgorithm="0" labelsEnabled="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endExpression="" accumulate="0" enabled="0" startField="" durationUnit="min" fixedDuration="0" limitMode="0" endField="" durationField="" mode="0" startExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation showMarkerSymbolInSurfacePlots="0" zoffset="0" symbology="Line" extrusion="0" clamping="Relative" zscale="1" binding="Centroid" type="IndividualFeatures" extrusionEnabled="0" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol name="" frame_rate="10" alpha="1" type="line" clip_to_extent="1" is_animated="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="114,155,111,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.6" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol name="" frame_rate="10" alpha="1" type="fill" clip_to_extent="1" is_animated="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" locked="0" class="SimpleFill">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
            <Option value="114,155,111,255" name="color" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="81,111,79,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="solid" name="style" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol name="" frame_rate="10" alpha="1" type="marker" clip_to_extent="1" is_animated="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" locked="0" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" name="angle" type="QString"/>
            <Option value="square" name="cap_style" type="QString"/>
            <Option value="114,155,111,255" name="color" type="QString"/>
            <Option value="1" name="horizontal_anchor_point" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="diamond" name="name" type="QString"/>
            <Option value="0,0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="81,111,79,255" name="outline_color" type="QString"/>
            <Option value="solid" name="outline_style" type="QString"/>
            <Option value="0.2" name="outline_width" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
            <Option value="MM" name="outline_width_unit" type="QString"/>
            <Option value="diameter" name="scale_method" type="QString"/>
            <Option value="3" name="size" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
            <Option value="MM" name="size_unit" type="QString"/>
            <Option value="1" name="vertical_anchor_point" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 forceraster="0" enableorderby="0" referencescale="-1" type="singleSymbol" symbollevels="0">
    <symbols>
      <symbol name="0" frame_rate="10" alpha="1" type="fill" clip_to_extent="1" is_animated="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option value="0" name="align_dash_pattern" type="QString"/>
            <Option value="square" name="capstyle" type="QString"/>
            <Option value="5;2" name="customdash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
            <Option value="MM" name="customdash_unit" type="QString"/>
            <Option value="0" name="dash_pattern_offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
            <Option value="0" name="draw_inside_polygon" type="QString"/>
            <Option value="bevel" name="joinstyle" type="QString"/>
            <Option value="228,26,28,255" name="line_color" type="QString"/>
            <Option value="solid" name="line_style" type="QString"/>
            <Option value="0.36" name="line_width" type="QString"/>
            <Option value="MM" name="line_width_unit" type="QString"/>
            <Option value="0" name="offset" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
            <Option value="MM" name="offset_unit" type="QString"/>
            <Option value="0" name="ring_filter" type="QString"/>
            <Option value="0" name="trim_distance_end" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_end_unit" type="QString"/>
            <Option value="0" name="trim_distance_start" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
            <Option value="MM" name="trim_distance_start_unit" type="QString"/>
            <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
            <Option value="0" name="use_custom_dash" type="QString"/>
            <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" allowHtml="0" previewBkgrdColor="255,255,255,255" capitalization="0" fontSize="10" forcedItalic="0" textColor="50,50,50,255" textOrientation="horizontal" fontItalic="0" fontKerning="1" forcedBold="0" multilineHeightUnit="Percentage" fontWordSpacing="0" namedStyle="Regular" fontUnderline="0" fontSizeUnit="Point" blendMode="0" textOpacity="1" fontLetterSpacing="0" fieldName="mappale" legendString="Aa" fontSizeMapUnitScale="3x:0,0,0,0,0,0" isExpression="0" fontWeight="50" useSubstitutions="0" fontFamily="Open Sans" fontStrikeout="0">
        <families/>
        <text-buffer bufferColor="250,250,250,255" bufferBlendMode="0" bufferSize="1" bufferNoFill="1" bufferOpacity="1" bufferJoinStyle="128" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSizeUnits="MM" bufferDraw="1"/>
        <text-mask maskJoinStyle="128" maskSize="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskOpacity="1" maskType="0" maskSizeUnits="MM" maskedSymbolLayers="" maskEnabled="0"/>
        <background shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1" shapeJoinStyle="64" shapeOffsetX="0" shapeBorderWidthUnit="Point" shapeBlendMode="0" shapeOffsetY="0" shapeType="0" shapeRadiiX="0" shapeRotation="0" shapeBorderColor="128,128,128,255" shapeDraw="0" shapeRadiiUnit="Point" shapeSizeY="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeSizeType="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidth="0" shapeOffsetUnit="Point" shapeRotationType="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="Point" shapeSizeX="0">
          <symbol name="markerSymbol" frame_rate="10" alpha="1" type="marker" clip_to_extent="1" is_animated="0" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" enabled="1" locked="0" class="SimpleMarker">
              <Option type="Map">
                <Option value="0" name="angle" type="QString"/>
                <Option value="square" name="cap_style" type="QString"/>
                <Option value="152,125,183,255" name="color" type="QString"/>
                <Option value="1" name="horizontal_anchor_point" type="QString"/>
                <Option value="bevel" name="joinstyle" type="QString"/>
                <Option value="circle" name="name" type="QString"/>
                <Option value="0,0" name="offset" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                <Option value="MM" name="offset_unit" type="QString"/>
                <Option value="35,35,35,255" name="outline_color" type="QString"/>
                <Option value="solid" name="outline_style" type="QString"/>
                <Option value="0" name="outline_width" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale" type="QString"/>
                <Option value="MM" name="outline_width_unit" type="QString"/>
                <Option value="diameter" name="scale_method" type="QString"/>
                <Option value="2" name="size" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="size_map_unit_scale" type="QString"/>
                <Option value="MM" name="size_unit" type="QString"/>
                <Option value="1" name="vertical_anchor_point" type="QString"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol name="fillSymbol" frame_rate="10" alpha="1" type="fill" clip_to_extent="1" is_animated="0" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" enabled="1" locked="0" class="SimpleFill">
              <Option type="Map">
                <Option value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale" type="QString"/>
                <Option value="255,255,255,255" name="color" type="QString"/>
                <Option value="bevel" name="joinstyle" type="QString"/>
                <Option value="0,0" name="offset" type="QString"/>
                <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
                <Option value="MM" name="offset_unit" type="QString"/>
                <Option value="128,128,128,255" name="outline_color" type="QString"/>
                <Option value="no" name="outline_style" type="QString"/>
                <Option value="0" name="outline_width" type="QString"/>
                <Option value="Point" name="outline_width_unit" type="QString"/>
                <Option value="solid" name="style" type="QString"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowUnder="0" shadowOffsetDist="1" shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowBlendMode="6" shadowRadius="1.5" shadowRadiusUnit="MM" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowOpacity="0.69999999999999996" shadowOffsetUnit="MM" shadowOffsetAngle="135"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format placeDirectionSymbol="0" multilineAlign="3" reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" formatNumbers="0" addDirectionSymbol="0" plussign="0" autoWrapLength="0" wrapChar="" rightDirectionSymbol=">" leftDirectionSymbol="&lt;" decimals="3"/>
      <placement geometryGenerator="" rotationAngle="0" centroidInside="0" fitInPolygonOnly="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" dist="0" maxCurvedCharAngleIn="25" overlapHandling="PreventOverlap" centroidWhole="0" lineAnchorPercent="0.5" geometryGeneratorType="PointGeometry" lineAnchorTextPoint="FollowPlacement" allowDegraded="0" yOffset="0" placementFlags="10" offsetType="0" overrunDistanceUnit="MM" repeatDistance="0" layerType="PolygonGeometry" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorEnabled="0" overrunDistance="0" placement="0" lineAnchorClipping="0" rotationUnit="AngleDegrees" distUnits="MM" lineAnchorType="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" preserveRotation="1" distMapUnitScale="3x:0,0,0,0,0,0" polygonPlacementFlags="2" quadOffset="4" xOffset="0" offsetUnits="MM" priority="5" maxCurvedCharAngleOut="-25" repeatDistanceUnits="MM"/>
      <rendering fontMinPixelSize="3" maxNumLabels="2000" fontLimitPixelSize="0" drawLabels="1" unplacedVisibility="0" obstacleType="1" scaleVisibility="0" obstacle="1" upsidedownLabels="0" obstacleFactor="1" fontMaxPixelSize="10000" zIndex="0" minFeatureSize="0" labelPerPart="0" limitNumLabels="0" scaleMin="0" scaleMax="0" mergeLines="0"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" name="name" type="QString"/>
          <Option name="properties"/>
          <Option value="collection" name="type" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
          <Option value="0" name="blendMode" type="int"/>
          <Option name="ddProperties" type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
          <Option value="false" name="drawToAllParts" type="bool"/>
          <Option value="0" name="enabled" type="QString"/>
          <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
          <Option value="&lt;symbol name=&quot;symbol&quot; frame_rate=&quot;10&quot; alpha=&quot;1&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; is_animated=&quot;0&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; enabled=&quot;1&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;0&quot; name=&quot;align_dash_pattern&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;square&quot; name=&quot;capstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;5;2&quot; name=&quot;customdash&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;customdash_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;customdash_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;dash_pattern_offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;dash_pattern_offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;draw_inside_polygon&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;bevel&quot; name=&quot;joinstyle&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;60,60,60,255&quot; name=&quot;line_color&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;solid&quot; name=&quot;line_style&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0.3&quot; name=&quot;line_width&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;line_width_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;offset&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;offset_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;ring_filter&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;trim_distance_end&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_end_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;trim_distance_end_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;trim_distance_start&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_start_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;MM&quot; name=&quot;trim_distance_start_unit&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;tweak_dash_pattern_on_corners&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;0&quot; name=&quot;use_custom_dash&quot; type=&quot;QString&quot;/>&lt;Option value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;width_map_unit_scale&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
          <Option value="0" name="minLength" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
          <Option value="MM" name="minLengthUnit" type="QString"/>
          <Option value="0" name="offsetFromAnchor" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
          <Option value="0" name="offsetFromLabel" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option value="false" name="OnConvertFormatRegeneratePrimaryKey" type="bool"/>
      <Option value="0" name="embeddedWidgets/count" type="int"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory spacingUnitScale="3x:0,0,0,0,0,0" height="15" minScaleDenominator="0" penWidth="0" direction="0" penAlpha="255" spacing="5" sizeType="MM" rotationOffset="270" backgroundAlpha="255" scaleBasedVisibility="0" penColor="#000000" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" lineSizeScale="3x:0,0,0,0,0,0" width="15" opacity="1" maxScaleDenominator="1e+08" scaleDependency="Area" spacingUnit="MM" showAxis="1" backgroundColor="#ffffff" diagramOrientation="Up" barWidth="5" enabled="0" lineSizeType="MM" minimumSize="0">
      <fontProperties bold="0" strikethrough="0" underline="0" italic="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <axisSymbol>
        <symbol name="" frame_rate="10" alpha="1" type="line" clip_to_extent="1" is_animated="0" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" enabled="1" locked="0" class="SimpleLine">
            <Option type="Map">
              <Option value="0" name="align_dash_pattern" type="QString"/>
              <Option value="square" name="capstyle" type="QString"/>
              <Option value="5;2" name="customdash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale" type="QString"/>
              <Option value="MM" name="customdash_unit" type="QString"/>
              <Option value="0" name="dash_pattern_offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="dash_pattern_offset_unit" type="QString"/>
              <Option value="0" name="draw_inside_polygon" type="QString"/>
              <Option value="bevel" name="joinstyle" type="QString"/>
              <Option value="35,35,35,255" name="line_color" type="QString"/>
              <Option value="solid" name="line_style" type="QString"/>
              <Option value="0.26" name="line_width" type="QString"/>
              <Option value="MM" name="line_width_unit" type="QString"/>
              <Option value="0" name="offset" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="offset_map_unit_scale" type="QString"/>
              <Option value="MM" name="offset_unit" type="QString"/>
              <Option value="0" name="ring_filter" type="QString"/>
              <Option value="0" name="trim_distance_end" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_end_unit" type="QString"/>
              <Option value="0" name="trim_distance_start" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale" type="QString"/>
              <Option value="MM" name="trim_distance_start_unit" type="QString"/>
              <Option value="0" name="tweak_dash_pattern_on_corners" type="QString"/>
              <Option value="0" name="use_custom_dash" type="QString"/>
              <Option value="3x:0,0,0,0,0,0" name="width_map_unit_scale" type="QString"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" obstacle="0" placement="1" priority="0" dist="0" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option value="0" name="allowedGapsBuffer" type="double"/>
        <Option value="false" name="allowedGapsEnabled" type="bool"/>
        <Option value="" name="allowedGapsLayer" type="QString"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rwn" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cntgeom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="idmappa" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="id" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="foglio_mappale" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mappalealice" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="allegatosviluppo" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codfoglio" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codicemappa" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="duplicati" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nome" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nomecdf" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="superficie" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geometry1_sk" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="comune" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codice_comune" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="comune_censuario" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sezione" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="foglio" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mappale" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="subalterno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tipo_catasto" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codice_particella_sub" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codparticella" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codparticellaf" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codice_immobile" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codice_immobile_catasto" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reddito_agrario" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reddito_dominicale" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="superficie_ettari" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="superficie_are" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="superficie_centiare" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="codice_qualita" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="descrizione_qualita" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="classe" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="stadio" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="data_validita_i" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="data_iscrizione" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tipo_nota" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="descrizione" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nota" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="annotazioni" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elenco_diritti_sogg1" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elenco_diritti_sogg2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="elenco_diritti_sogg3" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="rwn"/>
    <alias index="2" name="" field="cntgeom"/>
    <alias index="3" name="" field="idmappa"/>
    <alias index="4" name="" field="id"/>
    <alias index="5" name="" field="foglio_mappale"/>
    <alias index="6" name="" field="mappalealice"/>
    <alias index="7" name="" field="allegatosviluppo"/>
    <alias index="8" name="" field="codfoglio"/>
    <alias index="9" name="" field="codicemappa"/>
    <alias index="10" name="" field="duplicati"/>
    <alias index="11" name="" field="nome"/>
    <alias index="12" name="" field="nomecdf"/>
    <alias index="13" name="" field="superficie"/>
    <alias index="14" name="" field="geometry1_sk"/>
    <alias index="15" name="" field="comune"/>
    <alias index="16" name="" field="codice_comune"/>
    <alias index="17" name="" field="comune_censuario"/>
    <alias index="18" name="" field="sezione"/>
    <alias index="19" name="" field="foglio"/>
    <alias index="20" name="" field="mappale"/>
    <alias index="21" name="" field="subalterno"/>
    <alias index="22" name="" field="tipo_catasto"/>
    <alias index="23" name="" field="codice_particella_sub"/>
    <alias index="24" name="" field="codparticella"/>
    <alias index="25" name="" field="codparticellaf"/>
    <alias index="26" name="" field="codice_immobile"/>
    <alias index="27" name="" field="codice_immobile_catasto"/>
    <alias index="28" name="" field="reddito_agrario"/>
    <alias index="29" name="" field="reddito_dominicale"/>
    <alias index="30" name="" field="superficie_ettari"/>
    <alias index="31" name="" field="superficie_are"/>
    <alias index="32" name="" field="superficie_centiare"/>
    <alias index="33" name="" field="codice_qualita"/>
    <alias index="34" name="" field="descrizione_qualita"/>
    <alias index="35" name="" field="classe"/>
    <alias index="36" name="" field="stadio"/>
    <alias index="37" name="" field="data_validita_i"/>
    <alias index="38" name="" field="data_iscrizione"/>
    <alias index="39" name="" field="tipo_nota"/>
    <alias index="40" name="" field="descrizione"/>
    <alias index="41" name="" field="nota"/>
    <alias index="42" name="" field="annotazioni"/>
    <alias index="43" name="" field="elenco_diritti_sogg1"/>
    <alias index="44" name="" field="elenco_diritti_sogg2"/>
    <alias index="45" name="" field="elenco_diritti_sogg3"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="rwn" expression="" applyOnUpdate="0"/>
    <default field="cntgeom" expression="" applyOnUpdate="0"/>
    <default field="idmappa" expression="" applyOnUpdate="0"/>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="foglio_mappale" expression="" applyOnUpdate="0"/>
    <default field="mappalealice" expression="" applyOnUpdate="0"/>
    <default field="allegatosviluppo" expression="" applyOnUpdate="0"/>
    <default field="codfoglio" expression="" applyOnUpdate="0"/>
    <default field="codicemappa" expression="" applyOnUpdate="0"/>
    <default field="duplicati" expression="" applyOnUpdate="0"/>
    <default field="nome" expression="" applyOnUpdate="0"/>
    <default field="nomecdf" expression="" applyOnUpdate="0"/>
    <default field="superficie" expression="" applyOnUpdate="0"/>
    <default field="geometry1_sk" expression="" applyOnUpdate="0"/>
    <default field="comune" expression="" applyOnUpdate="0"/>
    <default field="codice_comune" expression="" applyOnUpdate="0"/>
    <default field="comune_censuario" expression="" applyOnUpdate="0"/>
    <default field="sezione" expression="" applyOnUpdate="0"/>
    <default field="foglio" expression="" applyOnUpdate="0"/>
    <default field="mappale" expression="" applyOnUpdate="0"/>
    <default field="subalterno" expression="" applyOnUpdate="0"/>
    <default field="tipo_catasto" expression="" applyOnUpdate="0"/>
    <default field="codice_particella_sub" expression="" applyOnUpdate="0"/>
    <default field="codparticella" expression="" applyOnUpdate="0"/>
    <default field="codparticellaf" expression="" applyOnUpdate="0"/>
    <default field="codice_immobile" expression="" applyOnUpdate="0"/>
    <default field="codice_immobile_catasto" expression="" applyOnUpdate="0"/>
    <default field="reddito_agrario" expression="" applyOnUpdate="0"/>
    <default field="reddito_dominicale" expression="" applyOnUpdate="0"/>
    <default field="superficie_ettari" expression="" applyOnUpdate="0"/>
    <default field="superficie_are" expression="" applyOnUpdate="0"/>
    <default field="superficie_centiare" expression="" applyOnUpdate="0"/>
    <default field="codice_qualita" expression="" applyOnUpdate="0"/>
    <default field="descrizione_qualita" expression="" applyOnUpdate="0"/>
    <default field="classe" expression="" applyOnUpdate="0"/>
    <default field="stadio" expression="" applyOnUpdate="0"/>
    <default field="data_validita_i" expression="" applyOnUpdate="0"/>
    <default field="data_iscrizione" expression="" applyOnUpdate="0"/>
    <default field="tipo_nota" expression="" applyOnUpdate="0"/>
    <default field="descrizione" expression="" applyOnUpdate="0"/>
    <default field="nota" expression="" applyOnUpdate="0"/>
    <default field="annotazioni" expression="" applyOnUpdate="0"/>
    <default field="elenco_diritti_sogg1" expression="" applyOnUpdate="0"/>
    <default field="elenco_diritti_sogg2" expression="" applyOnUpdate="0"/>
    <default field="elenco_diritti_sogg3" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="fid" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="rwn" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="cntgeom" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="idmappa" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="id" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="foglio_mappale" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="mappalealice" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="allegatosviluppo" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codfoglio" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codicemappa" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="duplicati" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="nome" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="nomecdf" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="superficie" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="geometry1_sk" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="comune" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codice_comune" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="comune_censuario" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="sezione" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="foglio" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="mappale" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="subalterno" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="tipo_catasto" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codice_particella_sub" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codparticella" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codparticellaf" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codice_immobile" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codice_immobile_catasto" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="reddito_agrario" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="reddito_dominicale" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="superficie_ettari" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="superficie_are" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="superficie_centiare" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="codice_qualita" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="descrizione_qualita" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="classe" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="stadio" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="data_validita_i" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="data_iscrizione" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="tipo_nota" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="descrizione" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="nota" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="annotazioni" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="elenco_diritti_sogg1" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="elenco_diritti_sogg2" unique_strength="0"/>
    <constraint constraints="0" notnull_strength="0" exp_strength="0" field="elenco_diritti_sogg3" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="fid" exp=""/>
    <constraint desc="" field="rwn" exp=""/>
    <constraint desc="" field="cntgeom" exp=""/>
    <constraint desc="" field="idmappa" exp=""/>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="foglio_mappale" exp=""/>
    <constraint desc="" field="mappalealice" exp=""/>
    <constraint desc="" field="allegatosviluppo" exp=""/>
    <constraint desc="" field="codfoglio" exp=""/>
    <constraint desc="" field="codicemappa" exp=""/>
    <constraint desc="" field="duplicati" exp=""/>
    <constraint desc="" field="nome" exp=""/>
    <constraint desc="" field="nomecdf" exp=""/>
    <constraint desc="" field="superficie" exp=""/>
    <constraint desc="" field="geometry1_sk" exp=""/>
    <constraint desc="" field="comune" exp=""/>
    <constraint desc="" field="codice_comune" exp=""/>
    <constraint desc="" field="comune_censuario" exp=""/>
    <constraint desc="" field="sezione" exp=""/>
    <constraint desc="" field="foglio" exp=""/>
    <constraint desc="" field="mappale" exp=""/>
    <constraint desc="" field="subalterno" exp=""/>
    <constraint desc="" field="tipo_catasto" exp=""/>
    <constraint desc="" field="codice_particella_sub" exp=""/>
    <constraint desc="" field="codparticella" exp=""/>
    <constraint desc="" field="codparticellaf" exp=""/>
    <constraint desc="" field="codice_immobile" exp=""/>
    <constraint desc="" field="codice_immobile_catasto" exp=""/>
    <constraint desc="" field="reddito_agrario" exp=""/>
    <constraint desc="" field="reddito_dominicale" exp=""/>
    <constraint desc="" field="superficie_ettari" exp=""/>
    <constraint desc="" field="superficie_are" exp=""/>
    <constraint desc="" field="superficie_centiare" exp=""/>
    <constraint desc="" field="codice_qualita" exp=""/>
    <constraint desc="" field="descrizione_qualita" exp=""/>
    <constraint desc="" field="classe" exp=""/>
    <constraint desc="" field="stadio" exp=""/>
    <constraint desc="" field="data_validita_i" exp=""/>
    <constraint desc="" field="data_iscrizione" exp=""/>
    <constraint desc="" field="tipo_nota" exp=""/>
    <constraint desc="" field="descrizione" exp=""/>
    <constraint desc="" field="nota" exp=""/>
    <constraint desc="" field="annotazioni" exp=""/>
    <constraint desc="" field="elenco_diritti_sogg1" exp=""/>
    <constraint desc="" field="elenco_diritti_sogg2" exp=""/>
    <constraint desc="" field="elenco_diritti_sogg3" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" name="fid" width="-1" type="field"/>
      <column hidden="0" name="rwn" width="-1" type="field"/>
      <column hidden="0" name="cntgeom" width="-1" type="field"/>
      <column hidden="0" name="idmappa" width="-1" type="field"/>
      <column hidden="0" name="id" width="-1" type="field"/>
      <column hidden="0" name="foglio_mappale" width="-1" type="field"/>
      <column hidden="0" name="mappalealice" width="-1" type="field"/>
      <column hidden="0" name="allegatosviluppo" width="-1" type="field"/>
      <column hidden="0" name="codfoglio" width="-1" type="field"/>
      <column hidden="0" name="codicemappa" width="-1" type="field"/>
      <column hidden="0" name="duplicati" width="-1" type="field"/>
      <column hidden="0" name="nome" width="-1" type="field"/>
      <column hidden="0" name="nomecdf" width="-1" type="field"/>
      <column hidden="0" name="superficie" width="-1" type="field"/>
      <column hidden="0" name="geometry1_sk" width="-1" type="field"/>
      <column hidden="0" name="comune" width="-1" type="field"/>
      <column hidden="0" name="codice_comune" width="-1" type="field"/>
      <column hidden="0" name="comune_censuario" width="-1" type="field"/>
      <column hidden="0" name="sezione" width="-1" type="field"/>
      <column hidden="0" name="foglio" width="-1" type="field"/>
      <column hidden="0" name="mappale" width="-1" type="field"/>
      <column hidden="0" name="subalterno" width="-1" type="field"/>
      <column hidden="0" name="tipo_catasto" width="-1" type="field"/>
      <column hidden="0" name="codice_particella_sub" width="-1" type="field"/>
      <column hidden="0" name="codparticella" width="-1" type="field"/>
      <column hidden="0" name="codparticellaf" width="-1" type="field"/>
      <column hidden="0" name="codice_immobile" width="-1" type="field"/>
      <column hidden="0" name="codice_immobile_catasto" width="-1" type="field"/>
      <column hidden="0" name="reddito_agrario" width="-1" type="field"/>
      <column hidden="0" name="reddito_dominicale" width="-1" type="field"/>
      <column hidden="0" name="superficie_ettari" width="-1" type="field"/>
      <column hidden="0" name="superficie_are" width="-1" type="field"/>
      <column hidden="0" name="superficie_centiare" width="-1" type="field"/>
      <column hidden="0" name="codice_qualita" width="-1" type="field"/>
      <column hidden="0" name="descrizione_qualita" width="-1" type="field"/>
      <column hidden="0" name="classe" width="-1" type="field"/>
      <column hidden="0" name="stadio" width="-1" type="field"/>
      <column hidden="0" name="data_validita_i" width="-1" type="field"/>
      <column hidden="0" name="data_iscrizione" width="-1" type="field"/>
      <column hidden="0" name="tipo_nota" width="-1" type="field"/>
      <column hidden="0" name="descrizione" width="-1" type="field"/>
      <column hidden="0" name="nota" width="-1" type="field"/>
      <column hidden="0" name="annotazioni" width="-1" type="field"/>
      <column hidden="0" name="elenco_diritti_sogg1" width="-1" type="field"/>
      <column hidden="0" name="elenco_diritti_sogg2" width="-1" type="field"/>
      <column hidden="0" name="elenco_diritti_sogg3" width="-1" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
I moduli di QGIS possono avere una funzione Python che può essere chiamata quando un modulo viene aperto.

Usa questa funzione per aggiungere logica extra ai tuoi moduli.

Inserisci il nome della funzione nel campo "Funzione Python di avvio".

Segue un esempio:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
geom = feature.geometry()
control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="allegatosviluppo" editable="1"/>
    <field name="annotazioni" editable="1"/>
    <field name="classe" editable="1"/>
    <field name="cntgeom" editable="1"/>
    <field name="codfoglio" editable="1"/>
    <field name="codice_comune" editable="1"/>
    <field name="codice_immobile" editable="1"/>
    <field name="codice_immobile_catasto" editable="1"/>
    <field name="codice_particella_sub" editable="1"/>
    <field name="codice_qualita" editable="1"/>
    <field name="codicemappa" editable="1"/>
    <field name="codparticella" editable="1"/>
    <field name="codparticellaf" editable="1"/>
    <field name="comune" editable="1"/>
    <field name="comune_censuario" editable="1"/>
    <field name="data_iscrizione" editable="1"/>
    <field name="data_validita_i" editable="1"/>
    <field name="descrizione" editable="1"/>
    <field name="descrizione_qualita" editable="1"/>
    <field name="duplicati" editable="1"/>
    <field name="elenco_diritti_sogg1" editable="1"/>
    <field name="elenco_diritti_sogg2" editable="1"/>
    <field name="elenco_diritti_sogg3" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="foglio" editable="1"/>
    <field name="foglio_mappale" editable="1"/>
    <field name="geometry1_sk" editable="1"/>
    <field name="id" editable="1"/>
    <field name="idmappa" editable="1"/>
    <field name="mappale" editable="1"/>
    <field name="mappalealice" editable="1"/>
    <field name="nome" editable="1"/>
    <field name="nomecdf" editable="1"/>
    <field name="nota" editable="1"/>
    <field name="reddito_agrario" editable="1"/>
    <field name="reddito_dominicale" editable="1"/>
    <field name="rwn" editable="1"/>
    <field name="sezione" editable="1"/>
    <field name="stadio" editable="1"/>
    <field name="subalterno" editable="1"/>
    <field name="superficie" editable="1"/>
    <field name="superficie_are" editable="1"/>
    <field name="superficie_centiare" editable="1"/>
    <field name="superficie_ettari" editable="1"/>
    <field name="tipo_catasto" editable="1"/>
    <field name="tipo_nota" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="allegatosviluppo" labelOnTop="0"/>
    <field name="annotazioni" labelOnTop="0"/>
    <field name="classe" labelOnTop="0"/>
    <field name="cntgeom" labelOnTop="0"/>
    <field name="codfoglio" labelOnTop="0"/>
    <field name="codice_comune" labelOnTop="0"/>
    <field name="codice_immobile" labelOnTop="0"/>
    <field name="codice_immobile_catasto" labelOnTop="0"/>
    <field name="codice_particella_sub" labelOnTop="0"/>
    <field name="codice_qualita" labelOnTop="0"/>
    <field name="codicemappa" labelOnTop="0"/>
    <field name="codparticella" labelOnTop="0"/>
    <field name="codparticellaf" labelOnTop="0"/>
    <field name="comune" labelOnTop="0"/>
    <field name="comune_censuario" labelOnTop="0"/>
    <field name="data_iscrizione" labelOnTop="0"/>
    <field name="data_validita_i" labelOnTop="0"/>
    <field name="descrizione" labelOnTop="0"/>
    <field name="descrizione_qualita" labelOnTop="0"/>
    <field name="duplicati" labelOnTop="0"/>
    <field name="elenco_diritti_sogg1" labelOnTop="0"/>
    <field name="elenco_diritti_sogg2" labelOnTop="0"/>
    <field name="elenco_diritti_sogg3" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="foglio" labelOnTop="0"/>
    <field name="foglio_mappale" labelOnTop="0"/>
    <field name="geometry1_sk" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="idmappa" labelOnTop="0"/>
    <field name="mappale" labelOnTop="0"/>
    <field name="mappalealice" labelOnTop="0"/>
    <field name="nome" labelOnTop="0"/>
    <field name="nomecdf" labelOnTop="0"/>
    <field name="nota" labelOnTop="0"/>
    <field name="reddito_agrario" labelOnTop="0"/>
    <field name="reddito_dominicale" labelOnTop="0"/>
    <field name="rwn" labelOnTop="0"/>
    <field name="sezione" labelOnTop="0"/>
    <field name="stadio" labelOnTop="0"/>
    <field name="subalterno" labelOnTop="0"/>
    <field name="superficie" labelOnTop="0"/>
    <field name="superficie_are" labelOnTop="0"/>
    <field name="superficie_centiare" labelOnTop="0"/>
    <field name="superficie_ettari" labelOnTop="0"/>
    <field name="tipo_catasto" labelOnTop="0"/>
    <field name="tipo_nota" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="allegatosviluppo" reuseLastValue="0"/>
    <field name="annotazioni" reuseLastValue="0"/>
    <field name="classe" reuseLastValue="0"/>
    <field name="cntgeom" reuseLastValue="0"/>
    <field name="codfoglio" reuseLastValue="0"/>
    <field name="codice_comune" reuseLastValue="0"/>
    <field name="codice_immobile" reuseLastValue="0"/>
    <field name="codice_immobile_catasto" reuseLastValue="0"/>
    <field name="codice_particella_sub" reuseLastValue="0"/>
    <field name="codice_qualita" reuseLastValue="0"/>
    <field name="codicemappa" reuseLastValue="0"/>
    <field name="codparticella" reuseLastValue="0"/>
    <field name="codparticellaf" reuseLastValue="0"/>
    <field name="comune" reuseLastValue="0"/>
    <field name="comune_censuario" reuseLastValue="0"/>
    <field name="data_iscrizione" reuseLastValue="0"/>
    <field name="data_validita_i" reuseLastValue="0"/>
    <field name="descrizione" reuseLastValue="0"/>
    <field name="descrizione_qualita" reuseLastValue="0"/>
    <field name="duplicati" reuseLastValue="0"/>
    <field name="elenco_diritti_sogg1" reuseLastValue="0"/>
    <field name="elenco_diritti_sogg2" reuseLastValue="0"/>
    <field name="elenco_diritti_sogg3" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="foglio" reuseLastValue="0"/>
    <field name="foglio_mappale" reuseLastValue="0"/>
    <field name="geometry1_sk" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="idmappa" reuseLastValue="0"/>
    <field name="mappale" reuseLastValue="0"/>
    <field name="mappalealice" reuseLastValue="0"/>
    <field name="nome" reuseLastValue="0"/>
    <field name="nomecdf" reuseLastValue="0"/>
    <field name="nota" reuseLastValue="0"/>
    <field name="reddito_agrario" reuseLastValue="0"/>
    <field name="reddito_dominicale" reuseLastValue="0"/>
    <field name="rwn" reuseLastValue="0"/>
    <field name="sezione" reuseLastValue="0"/>
    <field name="stadio" reuseLastValue="0"/>
    <field name="subalterno" reuseLastValue="0"/>
    <field name="superficie" reuseLastValue="0"/>
    <field name="superficie_are" reuseLastValue="0"/>
    <field name="superficie_centiare" reuseLastValue="0"/>
    <field name="superficie_ettari" reuseLastValue="0"/>
    <field name="tipo_catasto" reuseLastValue="0"/>
    <field name="tipo_nota" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"descrizione_qualita"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
